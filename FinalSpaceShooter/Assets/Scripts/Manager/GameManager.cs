﻿using System;
using Spaceship;
using Enemy;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private RectTransform dialogRestart;
        [SerializeField] private RectTransform dialogPause;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private ScoreManager scoreManager;
        [SerializeField] private ItemSpecial itemSpecial;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        //[SerializeField] private SoundManager soundManager;
        public static GameManager Instance { get; private set; }
        public static bool GamePaused = false;


        public PlayerSpaceship spawnedPlayerShip;
        public ItemSpecial spawnedItemSpacial;
        private float padding = 1.0f;
        private float xEnemyMin;
        private float xEnemyMax;
        private float yEnemyMin;
        private float yEnemyMax;
        
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            
            startButton.onClick.AddListener(OnStartButtonClicked);
            restartButton.onClick.AddListener(OnStartButtonClicked);
            
            if (Instance == null)
            {
                Instance = this;
            }

            DontDestroyOnLoad(this);
            //soundManager.PlayBGM();
            //SoundManager.Instance.PlayBGM();
        }
        private void Start()
        {
            dialogRestart.gameObject.SetActive(false);
            dialogPause.gameObject.SetActive(false);

            //SoundManager.Instance.PlayBGM();
        }

        public void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            dialogRestart.gameObject.SetActive(false);
            StartGame();
        }

        public void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            SpawnItemSpacial();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (GamePaused)
                {
                    ResumeGame();
                }
                else
                {
                    PauseGame();
                }
            }   
        }
        public void PauseGame()
        {
            dialogPause.gameObject.SetActive(true);
            Time.timeScale = 0f;
            GamePaused = true;
        }

        public void ResumeGame()
        {
            dialogPause.gameObject.SetActive(false);
            Time.timeScale = 1f;
            GamePaused = false;
        }

        public void ReturnToMenu()
        {
            SceneManager.LoadScene("Menu");
            Time.timeScale = 1f;
            GamePaused = false;
        }
        
        
        private void SpawnPlayerSpaceship()
        {
            spawnedPlayerShip = Instantiate(playerSpaceship);
            spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            //SceneManager.LoadScene("Game2");
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            Camera gameCamera = Camera.main;
            xEnemyMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).x + padding;
            xEnemyMax = gameCamera.ViewportToWorldPoint(new Vector2(1, 0)).x - padding;
            yEnemyMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).x + padding;
            yEnemyMax = gameCamera.ViewportToWorldPoint(new Vector2(0, 1)).x - padding;
            var spawnedEnemyShip = Instantiate(enemySpaceship);
            spawnedEnemyShip.transform.position = new Vector2(Random.Range(xEnemyMin, xEnemyMax), 1);
            spawnedEnemyShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;

            //var enemyController = spawnedEnemyShip.GetComponent<EnemyController>();
            //enemyController.Init(spawnedPlayerShip);
        }

        private void SpawnItemSpacial()
        {
            Camera gameCamera = Camera.main;
            xEnemyMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).x + padding;
            xEnemyMax = gameCamera.ViewportToWorldPoint(new Vector2(1, 0)).x - padding;
            yEnemyMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).x + padding;
            yEnemyMax = gameCamera.ViewportToWorldPoint(new Vector2(0, 1)).x - padding;
            spawnedItemSpacial = Instantiate(itemSpecial, new Vector2(Random.Range(xEnemyMin, xEnemyMax), 1),Quaternion.identity);
            
         
        }

        private void OnEnemySpaceshipExploded()
        {
            //scoreManager.Score += 100;
            ScoreManager.Instance.Score += 100;
            SpawnEnemySpaceship();
        }

        private void Restart()
        {
            DestroyRemainingShips();
            dialogRestart.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingShips()
        {
            var remaningEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remaningEnemies)
            {
                Destroy(enemy);
            }
            var remaningPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remaningPlayer)
            {
                Destroy(player);
            }
        }

        

    }
}
