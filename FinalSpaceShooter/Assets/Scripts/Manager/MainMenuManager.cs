﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private GameObject panelMenuGame;
    [SerializeField] private GameObject panelSettingGame;
    [SerializeField] private GameObject panelCreditGame;

    private void Start()
    {
        panelMenuGame.SetActive(true);
        panelSettingGame.SetActive(false);
        panelCreditGame.SetActive(false);
        SoundManager.Instance.PlayBGM();
    }

    public void SettingMenu()
    {
        panelMenuGame.SetActive(false);
        panelSettingGame.SetActive(true);
    }

    public void CreditMenu()
    {
        panelMenuGame.SetActive(false);
        panelCreditGame.SetActive(true);
    }

    public void BackMenu()
    {
        panelMenuGame.SetActive(true);
        panelSettingGame.SetActive(false);
        panelCreditGame.SetActive(false);
    }



}
