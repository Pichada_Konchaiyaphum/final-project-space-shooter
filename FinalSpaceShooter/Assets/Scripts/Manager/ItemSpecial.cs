﻿using Player;
using Spaceship;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpecial : MonoBehaviour
{
    [SerializeField] private PlayerSpaceship playerSpaceship;
    [SerializeField] private PlayerController player;
    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            var remaningItemSpacial = GameObject.FindGameObjectsWithTag("ItemSpacial");
            foreach (var itemSpacial in remaningItemSpacial)
            {
                Destroy(itemSpacial);
            }

            //playerSpaceship.ModeSpacial();
            
            Debug.Log("IGetItem");
        }
    }
   
}
