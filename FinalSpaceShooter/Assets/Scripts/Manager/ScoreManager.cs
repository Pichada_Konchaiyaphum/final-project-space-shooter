﻿using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;

        public static ScoreManager Instance { get; private set; }
        private GameManager gameManager;
        private int playerScore;
        int score;

        public int Score
        {
            get
            {
                return this.score;
            }
            set
            {
                this.score = value;
                SetScore(Score);
            }
        }
        
        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
        }

        public void SetScore(int score)
        {
            scoreText.text = $"Score : {score}";
            playerScore = score;
        }
        
        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");

            if (Instance == null)
            {
                Instance = this;
            }

            DontDestroyOnLoad(this);
        }
        
        private void OnRestarted()
        {
            finalScoreText.text = $"Playerscore : {playerScore}";
            gameManager.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}


