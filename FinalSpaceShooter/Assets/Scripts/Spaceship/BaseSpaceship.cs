using UnityEngine;

namespace Spaceship
{
    public abstract class Basespaceship : MonoBehaviour
    {
        [SerializeField] protected Bullet defaultBullet;
        [SerializeField] protected BulletSpacial bulletSpacial;
        [SerializeField] protected Transform gunPosition;
        [SerializeField] protected Transform gunPosition1;
        [SerializeField] protected Transform gunPosition2;
        [SerializeField] protected AudioSource audioSource;
        
        public int Hp { get; protected set; }
        public float Speed { get; private set; }
        public Bullet Bullet { get; private set; }
        public BulletSpacial BulletSpacial { get; private set; }

        protected void Init(int hp, float speed, Bullet bullet,BulletSpacial bulletSpacial)
        {
            Hp = hp;
            Speed = speed;
            Bullet = bullet;
            BulletSpacial = bulletSpacial;
        }

        public abstract void Fire();
    }
}