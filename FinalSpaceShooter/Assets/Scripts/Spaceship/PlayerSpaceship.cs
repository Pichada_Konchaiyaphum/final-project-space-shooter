using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        private float startingTime = 15f;
        public bool time = false;


        //[SerializeField] private AudioClip playExplode;
        //[SerializeField] private float playExplodeSound = 0.5f;


        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            audioSource = gameObject.GetComponent<AudioSource>();
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet, bulletSpacial);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            var bullet1 = Instantiate(bulletSpacial, gunPosition1.position, Quaternion.identity);
            var bullet2 = Instantiate(bulletSpacial, gunPosition2.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            bullet1.Init(Vector2.up);
            bullet2.Init(Vector2.up);
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.PlayerFire);
             
        }



         
            
        






        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            //AudioSource.PlayClipAtPoint(playExplode, Camera.main.transform.position, playExplodeSound);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
            
        }

        public void Update()
        {
            
            
            
        }
    }
}