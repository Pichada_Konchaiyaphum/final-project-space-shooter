using Manager;
using System;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet,bulletSpacial);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            var remaningEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remaningEnemies)
            {
                Destroy(enemy);
            }
            //Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            // TODO: Implement this later
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }

        }
    }
}