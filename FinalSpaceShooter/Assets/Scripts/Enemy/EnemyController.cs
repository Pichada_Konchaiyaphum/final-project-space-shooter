﻿using System.Collections;
using System.Collections.Generic;
using Manager;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;
        //[SerializeField] private PlayerSpaceship playerSpaceship;

        private PlayerSpaceship spawnedPlayership;

        
        private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
        }

         private void MoveToPlayer()
         {
            // TODO: Implement this later

            spawnedPlayership = GameManager.Instance.spawnedPlayerShip;
            if (spawnedPlayership == null)
                return;
            var distanceToPlayer = Vector2.Distance(spawnedPlayership.transform.position, transform.position);
            //Debug.Log(distanceToPlayer);

            if (distanceToPlayer < chasingThresholdDistance)
            {
                var direction = (Vector2)(spawnedPlayership.transform.position - transform.position);
                direction.Normalize();
                var distance = direction * enemySpaceship.Speed * Time.deltaTime;
                gameObject.transform.Translate(distance);
            }else 
            if(distanceToPlayer ==2)
            {
                

            }
         }
    }    
}

